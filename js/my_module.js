﻿/**
 * @file
 * Display the modal dialog with the message received from PHP.
 */

(function ($, Drupal, drupalSettings) {

  var message = drupalSettings['message'];
  var myDialog = $('<div>' + message + '</div>').appendTo('body');

  Drupal.dialog(myDialog, {
    title: 'A title',
    buttons: [{
      text: 'Close',
      click: function() {
        $(this).dialog('close');
      }
    }]
  }).showModal();

} (jQuery, Drupal, drupalSettings));
